var clientesObtenidos;

function getClientes(){
	var url="https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
	var request=new XMLHttpRequest();
	request.onreadystatechange = function(){
		if(this.readyState == 4 && this.status== 200){
			//console.table(JSON.parse(request.responseText).value);
			clientesObtenidos=request.responseText;
			procesarClientes();
		}
	}
	request.open("GET", url, true);
	request.send();
}

function procesarClientes(){
	var flag="https://www.countries-ofthe-world.com/flags-normal/flag-of-"
	var JSONClientes=JSON.parse(clientesObtenidos);
	var divTabla=document.getElementById("tablaClientes");
	var tabla=document.createElement("table");
	var tbody=document.createElement("tbody");
	tabla.classList.add("table");
	tabla.classList.add("table-striped");
  for (var i = 0; i<JSONClientes.value.length; i++) {
  var nuevaFila=document.createElement("tr");
  var columnaContact=document.createElement("td");
  columnaContact.innerText=JSONClientes.value[i].ContactName;
  var columnaCity=document.createElement("td");
  columnaCity.innerText=JSONClientes.value[i].City;
  //var columnaBandera=document.createElement("td");
  var imgBandera=document.createElement("img");
  imgBandera.src=flag+JSONClientes.value[i].Country+".png";
  if(JSONClientes.value[i].Country=="UK"){
    imgBandera.src=flag+"United-Kingdom"+".png";
}
imgBandera.style.width="70px";
imgBandera.style.height="40px";
nuevaFila.appendChild(columnaContact);
nuevaFila.appendChild(columnaCity);
nuevaFila.appendChild(imgBandera);
tbody.appendChild(nuevaFila);
//console.log(JSONProductos.value[i].ProductName);
}
tabla.appendChild(tbody);
divTabla.appendChild(tabla);
}  
