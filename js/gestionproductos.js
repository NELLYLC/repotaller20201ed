var productosObtenidos;

function getProductos(){
  var url="https://services.odata.org/V4/Northwind/Northwind.svc/Products";
  var request=new XMLHttpRequest();

  request.onreadystatechange=function(){
    if (this.readyState == 4 && this.status == 200) {
      //console.table(JSON.parse(request.responseText).value);
      productosObtenidos=request.responseText;
      procesarProductos();
    }
  }
  request.open("GET",url,true);
  request.send();
}

function procesarProductos(){
	var JSONProductos=JSON.parse(productosObtenidos);
	var divTabla=document.getElementById("tablaProductos");
	var tabla=document.createElement("table");
	var tbody=document.createElement("tbody");

	tabla.classList.add("table");
	tabla.classList.add("table-striped");

  for (var i = 0; i<JSONProductos.value.length; i++) {
  var nuevaFila=document.createElement("tr");

  var columnaNombre=document.createElement("td");
  columnaNombre.innerText=JSONProductos.value[i].ProductName;

  var columnaPrecio=document.createElement("td");
  columnaPrecio.innerText=JSONProductos.value[i].UnitPrice;

  var columnaUnidades=document.createElement("td");
  columnaUnidades.innerText=JSONProductos.value[i].UnitsInStock;

nuevaFila.appendChild(columnaNombre);
nuevaFila.appendChild(columnaPrecio);
nuevaFila.appendChild(columnaUnidades);

tbody.appendChild(nuevaFila);

		//console.log(JSONProductos.value[i].ProductName);
	}
	tabla.appendChild(tbody);
	divTabla.appendChild(tabla);
}

/*
function procesarProductos(){
  var JSONProductos=JSON.parse(productosObtenidos);
  var divTabla=document.getElementById("tablaProductos");
  var tabla=document.createElement("table");
  var tbody=document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  //alert(JSONProductos.value(0).ProductName);

  for (var i = 0; i < JSONProductos.value.length; i++) {
    //console.log[JSONProductos.value(i).ProductName];
    var nuevaFila = document.createElement("tr");

    var columnaNombre = document.createElement("td");
    columnaNombre.innerText = JSONProductos.value[i].ProductoName;

    var columnaPrecio=document.createElement("td");
    columnaNombre.innerText = JSONProductos.value[i].UnitPrice;

    var columnaStock=document.createElement("td");
    columnaNombre.innerText = JSONProductos.value[i].UnitInStock;

    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaPrecio);
    nuevaFila.appendChild(columnaStock);

    tbody.appendChild(nuevaFila);

    }
    tabla.appendChild(tbody);
    divTabla.appendChild(tabla);
}
*/
